import sys
import os
import psycopg2
import psycopg2.extras
import pprint
from datetime import datetime
from .. import tx_id_generator

__location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))

def main():
    conn_string = "host='10.97.1.10' dbname='uloan' user='uloan_app'"
    conn = None
    tx_id_generator.read_last_tx_record_id()

    try:
        print "Connecting to database\n ->%s" % (conn_string)
        conn = psycopg2.connect(conn_string)

        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        print "Connected!\n"

        for line in open(os.path.join(__location__, 'loans2balances.txt')):
            splitted = line.split()
            balance = int(splitted[0])
            loanid = int(splitted[1])
            explore_loan(loanid, balance, cursor, conn)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        tx_id_generator.write_last_tx_record_id()
        if conn is not None:
            conn.close()
            print("Database connection closed.")


def explore_loan(loanid, balance, cursor, conn):
    print "----------------------------------------------\nexploring loan %d" % loanid

    percentage_rate = fetch_percentage_rate(cursor, loanid)

    print "fpercentage rate: %.2f" % percentage_rate

    cursor.execute("""
        select tr.*
        from transaction_record as tr
        left join account as ac on ac.accountid = tr.toaccount_accountid
        where ac.referencedloanid = {}
        ORDER BY transactiondatetime desc""".format(loanid))

    records = cursor.fetchall()

    disbursal = disbursal_from_records(records)
    
    print "disbursal: %.2f" % disbursal

    overdue_fee_records = filter(
            lambda rec: rec['description'] == 'Overdue fee',
            records)
    overdue_fees = amounts_only(overdue_fee_records)

    print "overdue_fees count: %d, overdue_fees sum: %.2f" % (
            len(overdue_fees), sum(overdue_fees))

    percentage_fee_records = filter(
            lambda rec: is_percentage(rec['amount'], disbursal, percentage_rate),
            overdue_fee_records)

    negative_fee_records = filter(
            lambda rec: rec['amount'] < 0,
            overdue_fee_records)

    print "percentage fees count: %d, percentage fees sum: %.2f" % (
            len(percentage_fee_records), sum(amounts_only(percentage_fee_records)))

    print "negative fees count: %d, negative fees sum: %.2f" % (
            len(negative_fee_records), sum(amounts_only(negative_fee_records)))

    single_negative = len(negative_fee_records) == 1
    negative_is_half_of_positive = sum(amounts_only(percentage_fee_records)) + sum(amounts_only(negative_fee_records)) * 2 == 0
    is_expected_count = len(overdue_fee_records) == 61
    is_expected_sum_of_fees = sum(overdue_fees) * -1 == balance

    if is_expected_count and single_negative and negative_is_half_of_positive and is_expected_sum_of_fees:
        try:
            insert_applied_fee(cursor, loanid, balance)
            insert_transaction_record(cursor, negative_fee_records)
            conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
    else:
        print "~~~~~~~~~~~~~~~~~~~~~~~~~ atypical loan ~~~~~~~~~~~~~~~~~~~~~~~~~\n\n"

    # pprint.pprint(records)

def insert_transaction_record(cursor, negative_fee_records):
    negative_record = negative_fee_records[0]
    date_to_insert = build_date_to_insert(negative_record['transactiondatetime'])
    tx_record_insertion_expr = """
        INSERT INTO transaction_record 
        SELECT {}, tr.amount,tr.fromaccount_accountid,tr.toaccount_accountid,tr.description,'{}',tr.transactiontype
        FROM transaction_record tr WHERE tr.transactionrecordid = {};
        """.format(tx_id_generator.next_tx_record_id(), date_to_insert, negative_record['transactionrecordid'])
    print "transaction record insertion expression:\n{}".format(tx_record_insertion_expr)
    # cursor.execute(tx_record_insertion_expr)

def insert_applied_fee(cursor, loanid, balance):
    cursor.execute("""
        select af.appliedfeeid, af.dateandtime, af.amount from applied_fee af where loan_loanid = {} and amount < 0
            """.format(loanid))
    negative_fees = cursor.fetchall()
    if len(negative_fees) != 1:
        raise Exception("there are more than one negative fees")
    negative_fee = negative_fees[0]
    if negative_fee['amount'] != balance:
        raise Exception("unexpected amount of negative fee (expected: {}, actual: {})".format(
            balance, negative_fee['amount']))
    date_to_insert = build_date_to_insert(negative_fee['dateandtime'])
    applied_fee_insertion_expr = """
        insert into applied_fee
        select nextval('applied_fee_seq'), amount, '{}', loan_loanid, fee_feeid
        from applied_fee af where af.appliedfeeid = {}
            """.format(date_to_insert, negative_fee['appliedfeeid'])

    print "applied fee insertion expression:\n{}".format(applied_fee_insertion_expr)

    # cursor.execute(applied_fee_insertion_expr)

def build_date_to_insert(date_from_record):
    pattern = "%Y-%m-%d %H:%M:%S.%f"
    #dt = datetime.strptime(date_from_record, pattern)
    now = datetime.now()
    #new_date = dt.replace(hour = now.month, minute = now.day)
    new_date = date_from_record.replace(hour = now.month, minute = now.day)
    return datetime.strftime(new_date, pattern)

def fetch_percentage_rate(cursor, loanid):
    cursor.execute("""
        select fee.percentagerate
        from fee
        join product_fee pf ON fee.feeid = pf.fees_feeid
        JOIN product p ON pf.product_productid = p.productid
        join loan on loan.product_productid = p.productid
        join fee_type ft ON fee.feetype_feetypeid = ft.feetypeid
        where ft.name = 'Overdue fee' and loan.loanid = """ + str(loanid))

    record = cursor.fetchone()
    return float(record['percentagerate'])

def disbursal_from_records(records):
    disbursal_records = filter(
            lambda rec: rec['description'] == 'Loan disbursal',
            records)
    disbursals = amounts_only(disbursal_records)
    return float(sum(disbursals))

def amounts_only(records):
    return map(
            lambda rec: rec['amount'],
            records)

def is_percentage(amount, total, percentage):
    return isclose(float(amount), total * (percentage / 100.00))

def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
        return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)

if __name__ == "__main__":
    main()
