import os
import psycopg2
import psycopg2.extras
from .. import tx_id_generator

__location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))


def main():
    conn_string = "host='10.97.1.10' dbname='uloan' user='uloan_app'"
    conn = None
    tx_id_generator.read_last_tx_record_id()

    try:
        print "Connecting to database\n ->%s" % (conn_string)
        conn = psycopg2.connect(conn_string)

        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        print "Connected!\n"

        for line in open(os.path.join(__location__, 'fees_to_insert.txt')):
            splitted = line.split("|")
            loanid = int(splitted[0])
	    dt_str = splitted[1]
            amount = float(splitted[2])
            insert_fee(loanid, dt_str, amount, cursor, conn)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        tx_id_generator.write_last_tx_record_id()
        if conn is not None:
            conn.close()
            print("Database connection closed.")

def insert_fee(loanid, dt_str, amount, cursor, conn):
    try:
        insert_applied_fee(loanid, dt_str, amount, cursor)
        insert_tx_record(loanid, dt_str, amount, cursor)
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

def insert_applied_fee(loanid, dt_str, amount, cursor):
    cursor.execute("""
        SELECT f.feeid FROM product p
        JOIN product_fee pf ON p.productid = pf.product_productid
        JOIN fee f ON pf.fees_feeid = f.feeid
        JOIN loan l ON p.productid = l.product_productid
        JOIN fee_type ft ON f.feetype_feetypeid = ft.feetypeid
        WHERE ft.feetypeid = 4 and ft.name = 'Overdue fee' and l.loanid = {}
        """.format(loanid))
    feeid_record = cursor.fetchone()
    feeid = feeid_record['feeid']
    fee_insertion_expr = """
        INSERT INTO applied_fee
        (appliedfeeid, amount, dateandtime, loan_loanid, fee_feeid, eps_process_id)
        VALUES
        (nextval('applied_fee_seq'),{},'{}',{},{},null);
        """.format(amount, dt_str, loanid, feeid)

    print "fee_insertion_expr: %s" % fee_insertion_expr
    cursor.execute(fee_insertion_expr)

def insert_tx_record(loanid, dt_str, amount, cursor):
    cursor.execute("""
        select a.accountid from account a where a.referencedloanid = {}
        """.format(loanid))
    accountid_record = cursor.fetchone()
    accountid = accountid_record['accountid']

    tx_record_insertion_expr = """
        INSERT INTO transaction_record
        (transactionrecordid, amount, fromaccount_accountid, toaccount_accountid, description, transactiondatetime,
         transactiontype, eps_process_id, remote_date)
         VALUES ({}, {}, 3, {},'Overdue fee','{}','APPLIED_FEE',null,null);
        """.format(tx_id_generator.next_tx_record_id(), amount, accountid, dt_str)

    print "tx_record_insertion_expr: %s" % tx_record_insertion_expr
    cursor.execute(tx_record_insertion_expr)


if __name__ == "__main__":
    main()

