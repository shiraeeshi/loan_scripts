import os

__location__ = os.path.realpath(
            os.path.join(os.getcwd(), os.path.dirname(__file__)))

free_transaction_record_ids = [
    24851,
    25127,
    25414,
    25583,
    25828,
    26085,
    26146,
    26241,
    26263,
    26344,
    26579,
    26636,
    26685,
    26693,
    26848,
    26911,
    26956,
    27019,
    27038,
    27280,
    27388,
    27426,
    27510,
    27561,
    27634,
    27810,
    28015,
    28080,
    28134,
    28188,
    28195,
    28217,
    28365,
    28406,
    28484,
    28526,
    28815,
    28910,
    29103,
    29169,
    29209,
    29303,
    29315,
    29497,
    29516,
    29581,
    29785,
    29790,
    29793,
    29801,
    30003,
    30175,
    30457,
    30737,
    30854,
    31134,
    31185,
    31206,
    31370,
    31407,
    31436,
    31515,
    31705,
    31948,
    31981,
    32080,
    32199,
    32473,
    32705,
    32858,
    33018,
    33400,
    33477,
    33862,
    33973,
    33978,
    33996,
    34082,
    34091,
    34484,
    34567,
    35075,
    35290,
    35337,
    35372,
    35752,
    35769,
    35787,
    36049,
    36055,
    36081,
    36311,
    36468,
    36624,
    37024,
    37262,
    37292,
    37407,
    37829,
    38013,
    38043,
    38057,
    38067,
    38128,
    38397,
    38554,
    38564,
    38706,
    38709,
    38742,
    38825,
    39062,
    39071,
    39154,
    39249,
    39524,
    39574,
    40225,
    40354,
    40448,
    40512,
    40540,
    40607,
    40745,
    40767,
    40807,
    40954,
    40971,
    40976,
    40982,
    40988,
    41152,
    41191,
    41275,
    41308,
    41330,
    41386,
    41431,
    41496,
    41601,
    41866,
    41887,
    41936,
    42206,
    42234,
    42346,
    42464,
    42661,
    42686,
    42787,
    43141,
    43174,
    43298,
    43669,
    43860,
    43928,
    43983,
    44088,
    44118,
    44148,
    44160,
    44343,
    44383,
    44553,
    44682,
    44700,
    44718,
    45043,
    45221,
    45231,
    45399,
    45522,
    45781,
    45795,
    46013,
    46027,
    46146,
    46231,
    46393,
    46416,
    46433,
    46444,
    46942,
    47014,
    47333,
    47351,
    47369,
    47576,
    47748,
    47794,
    47845,
    47852,
    47884,
    47923,
    48096,
    48131,
    48161,
    48314,
    48315,
    48471,
    48477,
    48617,
    48686,
    48715,
    48869,
    48998,
    49056,
    49059,
    49069,
    49074,
    49409,
    49653,
    49658,
    49731,
    49930,
    49934,
    49995,
    50066,
    50339,
    50420,
    50551,
    50655, # last used
    50965,
    51032,
    51160,
    51252,
    51343,
    51537,
    51681,
    51704,
    51803,
    52141,
    52190,
    52356,
    52379,
    52743,
    52811,
    52833,
    52938,
    52948,
    53055,
    53062,
    53077,
    53176,
    53438,
    53717,
    54121,
    54309,
    54490,
    54731,
    54813,
    54854,
    54904,
    55009,
    55069,
    55119,
    55312,
    55464,
    55998,
    56176,
    56185,
    56189,
    56196,
    56414,
    56522,
    56830,
    56879,
    56951,
    57020,
    57530,
    57620,
    57691,
    57770,
    58277,
    58309,
    58461,
    58725,
    59004,
    59292,
    59486,
    59756,
    60064,
    60168,
    60358,
    60531,
    60678,
    60736,
    60844,
    61021,
    61159,
    61160,
    61554,
    61611,
    61766,
    61768,
    61852,
    62162,
    62215,
    62426,
    62608,
    62611,
    62631,
    62762,
    62877,
    63108,
    63416,
    63428,
    63596,
    63650,
    63751,
    64090,
    64240,
    64486,
    64853,
    64891,
    64915,
    64994,
    65102,
    65136,
    65206,
    65211,
    65233,
    65253,
    65329 ]

def next_tx_record_id():
    last_tx_record_id_line.num += 1
    print "|||||||||||||||| last tx record id line num: %d" % last_tx_record_id_line.num
    return free_transaction_record_ids[last_tx_record_id_line.num]

class LineNum(object):
    def __init__(self):
        self.num = 0

last_tx_record_id_line = LineNum()

def read_last_tx_record_id():
    with open(os.path.join(__location__, 'last_tx_record_id_index.txt')) as f:
        last_tx_record_id_line.num = int(f.read())

def write_last_tx_record_id():
    with open(os.path.join(__location__, 'last_tx_record_id_index.txt'), "w") as f:
        f.write(str(last_tx_record_id_line.num))
